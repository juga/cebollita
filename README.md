# Cebollita!

Projecto de ejemplo para aprendizaje de creación de [Servicios Cebolla][].

Este projecto contiene un conjunto de ejemplos útiles sobre como crear
Servicios Cebolla usando la tecnologia [Docker][].

## Ejemplo de servicio web

Para empezar el container web:

```
$ docker build onion-web -t web

$ docker run -itd --name web -p 80:80 web

```

Para acceder en el container en una sessión [SSH][]:

```
$ sudo docker exec -it web /bin/bash
```

Para obtener la direción del servicio cebolla:

```
$ sudo docker exec -it web /bin/cat /var/lib/tor/my_web/hostname
```

## Ejemplo de acesso SSH

Para empeza el container [SSH][]:

```
$ docker build onion-ssh -t ssh

$ docker run -itd --name ssh -p 22:22 ssh

```

Para acceder en el container en una sessión [SSH][]:

```
$ sudo docker exec -it ssh /bin/bash
```

Para obtener la direción del servicio cebolla:

```
$ sudo docker exec -it ssh /bin/cat /var/lib/tor/my_ssh/hostname
```

[Servicios Cebolla]: https://community.torproject.org/es/onion-services/
[Docker]: https://docs.docker.com
[SSH]: https://es.wikipedia.org/wiki/Secure_Shell
