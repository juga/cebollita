# Aplicación web con Servicios Cebolla usando Flask y Tor

Creación:

    docker-compose build

Inicialización:

    docker-compose up -d

Para obtener la fecha del servicio cebolla:

    docker-compose exec -u debian-tor tor cat /var/lib/tor/app/hostname
