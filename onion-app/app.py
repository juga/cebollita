#!/usr/bin/env python3
#
# Aplicación web de ejemplo.
#

from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return '<h1>Cebollita te saluda!</h1>'
